class Student

  attr_accessor :courses, :first_name, :last_name


  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def enroll(new_course)
    if @courses.any? { |course| course.conflicts_with?(new_course) }
      raise 'Course time conflict.'
    end

    @courses << new_course unless @courses.include?(new_course)

    new_course.students << self
  end

  def course_load
    dept_credit = Hash.new(0)
    @courses.each do |course|
      dept_credit[course.department] += course.credits
    end

    dept_credit
  end

end
